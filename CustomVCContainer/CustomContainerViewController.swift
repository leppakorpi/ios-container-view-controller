//
//  CustomContainerViewController.swift
//  CustomVCContainer
//
//  Created by Toni Leppäkorpi on 02/02/16.
//

import UIKit

class TopBarView: UIView {
    
    override func intrinsicContentSize() -> CGSize {
        let app = UIApplication.sharedApplication()
        let statusBarHeight = app.statusBarHidden ? 0.0 : app.statusBarFrame.height
        
        guard let window = self.window else { return CGSize(width: 0.0, height: 44.0 + statusBarHeight) }
        if window.bounds.height < 400.0 {
            return CGSize(width: 0.0, height: 32.0 + statusBarHeight)
        } else {
            return CGSize(width: 0.0, height: 44.0 + statusBarHeight)
        }
    }
}

class BottomBarView: UIView {
    
    override func intrinsicContentSize() -> CGSize {
        guard let window = self.window else { return CGSize(width: 0.0, height: 80.0) }
        if window.bounds.height < 400.0 {
            return CGSize(width: 0.0, height: 20.0)
        } else {
            return CGSize(width: 0.0, height: 80.0)
        }
    }
}

class CustomContainerViewController: UIViewController, UITextFieldDelegate {

    deinit {
        unregisterKeyboardListeners()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        topBarHideConstraint.active = false
        bottomBarHideConstraint.active = false
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        registerKeyboardListeners()
        updateButtons()
    }
    
    override func viewDidDisappear(animated: Bool) {
        super.viewDidDisappear(animated)
        unregisterKeyboardListeners()
    }
    
    override func viewWillTransitionToSize(size: CGSize, withTransitionCoordinator coordinator: UIViewControllerTransitionCoordinator) {
        topBar.invalidateIntrinsicContentSize()
        bottomBar.invalidateIntrinsicContentSize()
        coordinator.animateAlongsideTransition(
            { coordinator in
                self.updateChildVCLayoutGuides()
            }, completion: nil)
    }
    
    var statusBarHidden = false
    
    @IBAction func toggleStatusBar(sender: AnyObject) {
        self.statusBarHidden = !self.statusBarHidden
        UIView.animateWithDuration(barAnimationDuration, animations: {
            self.setNeedsStatusBarAppearanceUpdate()
            self.topBar.invalidateIntrinsicContentSize()
            self.updateChildVCLayoutGuides()
            self.view.layoutIfNeeded()
        })
    }
    
    override func prefersStatusBarHidden() -> Bool {
        return self.statusBarHidden
    }
    
    override func preferredStatusBarUpdateAnimation() -> UIStatusBarAnimation {
        return .Slide
    }
    
    // MARK: Top Bar
    
    let barAnimationDuration = 0.3
    
    var topBarHidden = false
    @IBOutlet var topBar: UIView!
    @IBOutlet var topBarShowConstraint: NSLayoutConstraint!
    @IBOutlet var topBarHideConstraint: NSLayoutConstraint!
    
    @IBAction func toggleTopBar(sender: AnyObject) {
        if topBarHidden {
            self.showTopBarAnimated(true)
        } else {
            self.hideTopBarAnimated(true)
        }
    }
    
    func showTopBarAnimated(animated: Bool) {
        guard topBarHidden else { return }
        self.topBarHidden = false
        self.topBar.hidden = false
        
        if animated {
            self.topBarHideConstraint.active = false
            self.topBarShowConstraint.constant = -self.topBar.bounds.height
            self.topBarShowConstraint.active = true
            UIView.animateWithDuration(barAnimationDuration, animations:
                {
                    self.updateChildVCLayoutGuides()
                    self.topBarShowConstraint.constant = 0.0
                    self.view.layoutIfNeeded()
                }, completion: nil)
            
        } else {
            self.topBarHideConstraint.active = false
            self.topBarShowConstraint.constant = 0.0
            self.topBarShowConstraint.active = true
            self.updateChildVCLayoutGuides()
        }
    }
    
    func hideTopBarAnimated(animated: Bool) {
        guard !topBarHidden else { return }
        self.topBarHidden = true
        
        if animated {
            UIView.animateWithDuration(barAnimationDuration, animations:
                {
                    self.updateChildVCLayoutGuides()
                    self.topBarShowConstraint.constant = -self.topBar.bounds.height
                    self.view.layoutIfNeeded()
                }, completion: { finished in
                    self.topBarShowConstraint.active = false
                    self.topBarHideConstraint.active = true
                    self.topBar.hidden = true
            })
            
        } else {
            self.topBarShowConstraint.active = false
            self.topBarHideConstraint.active = true
            self.topBar.hidden = true
            self.updateChildVCLayoutGuides()
        }
    }
    
    
    // MARK: Bottom Bar
    
    var bottomBarHidden = false
    @IBOutlet var bottomBar: UIView!
    @IBOutlet var bottomBarShowConstraint: NSLayoutConstraint!
    @IBOutlet var bottomBarHideConstraint: NSLayoutConstraint!
    
    @IBAction func toggleBottomBar(sender: AnyObject) {
        if bottomBarHidden {
            self.showBottomBarAnimated(true)
        } else {
            self.hideBottomBarAnimated(true)
        }
    }
    
    func showBottomBarAnimated(animated: Bool) {
        guard bottomBarHidden else { return }
        self.bottomBarHidden = false
        self.bottomBar.hidden = false
        
        if animated {
            self.bottomBarHideConstraint.active = false
            self.bottomBarShowConstraint.constant = -self.bottomBar.bounds.height
            self.bottomBarShowConstraint.active = true
            UIView.animateWithDuration(barAnimationDuration, animations:
                {
                    self.updateChildVCLayoutGuides()
                    self.bottomBarShowConstraint.constant = 0.0
                    self.view.layoutIfNeeded()
                }, completion: nil)
            
        } else {
            self.bottomBarHideConstraint.active = false
            self.bottomBarShowConstraint.constant = 0.0
            self.bottomBarShowConstraint.active = true
            self.updateChildVCLayoutGuides()
        }
    }
    
    func hideBottomBarAnimated(animated: Bool) {
        guard !bottomBarHidden else { return }
        self.bottomBarHidden = true
        
        if animated {
            UIView.animateWithDuration(barAnimationDuration, animations:
                {
                    self.updateChildVCLayoutGuides()
                    self.bottomBarShowConstraint.constant = -self.bottomBar.bounds.height
                    self.view.layoutIfNeeded()
                }, completion: { finished in
                    self.bottomBarShowConstraint.active = false
                    self.bottomBarHideConstraint.active = true
                    self.bottomBar.hidden = true
            })
            
        } else {
            self.bottomBarShowConstraint.active = false
            self.bottomBarHideConstraint.active = true
            self.bottomBar.hidden = true
            self.updateChildVCLayoutGuides()
        }
    }
    
    
    // MARK: child vc
    
    @IBOutlet var boxVCButton: UIButton!
    @IBOutlet var tableVCButton: UIButton!
    @IBOutlet var hideVCButton: UIButton!
    @IBOutlet var childVCContainer: UIView!
    var childVC: UIViewController?
    
    @IBAction func showBoxVC() {
        let vc = BoxViewController()
        addChildVC(vc)
    }
    
    @IBAction func showTableVC() {
        let vc = storyboard!.instantiateViewControllerWithIdentifier("myTableVC")
        addChildVC(vc)
    }
    
    @IBAction func hideChildVC() {
        removeChildVC()
    }
    
    func updateButtons() {
        let hasChildVC = childVC != nil
        boxVCButton.enabled = !hasChildVC
        tableVCButton.enabled = !hasChildVC
        hideVCButton.enabled = hasChildVC
    }
    
    func addChildVC(vc: UIViewController) {
        guard childVC == nil else { return }
        
        self.childVC = vc
        currentChildVCInset = UIEdgeInsetsZero
        updateChildVCLayoutGuides()
        self.addChildViewController(vc)
        vc.view.frame = childVCContainer.bounds
        childVCContainer.addSubview(vc.view)
        
        vc.didMoveToParentViewController(self)
        
        updateButtons()
    }
    
    func removeChildVC() {
        guard let vc = childVC else { return }
        
        vc.willMoveToParentViewController(nil)
        vc.view.removeFromSuperview()
        vc.removeFromParentViewController()
        self.childVC = nil
        
        updateButtons()
    }
    
    var currentChildVCInset = UIEdgeInsetsZero
    
    func updateChildVCLayoutGuides() {
        guard let vc = childVC else { return }
        
        let app = UIApplication.sharedApplication()
        let statusBarHeight = app.statusBarHidden ? 0.0 : app.statusBarFrame.height
        
        let topGuideLength = topBarHidden ? statusBarHeight : self.topBar.intrinsicContentSize().height
        let bottomGuideLength = bottomBarHidden ? 0.0 : self.bottomBar.bounds.height
        vc.setTopLayoutGuideLength(topGuideLength)
        vc.setBottomLayoutGuideLength(bottomGuideLength)
        
        if let scrollView = vc.view as? UIScrollView {
            var newChildInset = UIEdgeInsetsMake(topGuideLength, 0.0, bottomGuideLength, 0.0)
            if keyboardIsVisible {
                if vc is UITableViewController && vc.parentViewController != nil {
                    // UITableViewController handles keyboard related bottom inset
                    newChildInset.bottom = 0.0
                } else {
                    newChildInset.bottom = keyboardRect.height
                }
            }
            
            if newChildInset != currentChildVCInset {
                let topInsetChange = newChildInset.top - currentChildVCInset.top
                let bottomInsetChange = newChildInset.bottom - currentChildVCInset.bottom
                //debugPrint("adjust child insets \(topInsetChange) \(bottomInsetChange) (abs \(newChildInset.top) \(newChildInset.bottom))")
                
                if topInsetChange != 0.0 {
                    scrollView.contentInset.top += topInsetChange
                    if scrollView.contentOffset.y == -currentChildVCInset.top {
                        // if at top, stay top
                        scrollView.contentOffset.y -= topInsetChange
                    }
                }
                if bottomInsetChange != 0.0 {
                    scrollView.contentInset.bottom += bottomInsetChange
                }
                currentChildVCInset = newChildInset
            }
        }
    }


    // MARK: Keyboard

    var keyboardListenersRegistered = false
    
    func registerKeyboardListeners() {
        if keyboardListenersRegistered { return }
        keyboardListenersRegistered = true
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "keyboardWillShow:", name: UIKeyboardWillShowNotification, object: nil)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "keyboardWillBeHidden:", name: UIKeyboardWillHideNotification, object: nil)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "KeyboardWillChangeFrame:", name: UIKeyboardWillChangeFrameNotification, object: nil)
    }
    
    func unregisterKeyboardListeners() {
        if !keyboardListenersRegistered { return }
        keyboardListenersRegistered = false
        
        NSNotificationCenter.defaultCenter().removeObserver(self, name: UIKeyboardWillShowNotification, object: nil)
        NSNotificationCenter.defaultCenter().removeObserver(self, name: UIKeyboardWillHideNotification, object: nil)
        NSNotificationCenter.defaultCenter().removeObserver(self, name: UIKeyboardWillChangeFrameNotification, object: nil)
    }
    
    var keyboardIsVisible = false
    var keyboardRect: CGRect = CGRectNull
    
    func keyboardWillShow(aNotification: NSNotification) {
        let info = aNotification.userInfo! as NSDictionary
        keyboardIsVisible = true
        keyboardRect = (info[UIKeyboardFrameEndUserInfoKey] as! NSValue).CGRectValue()
        updateChildVCLayoutGuides()
    }
    
    func keyboardWillBeHidden(aNotification: NSNotification) {
        keyboardIsVisible = false
        keyboardRect = CGRectNull
        updateChildVCLayoutGuides()
    }
    
    func KeyboardWillChangeFrame(aNotification: NSNotification) {
        let info = aNotification.userInfo! as NSDictionary
        keyboardRect = (info[UIKeyboardFrameEndUserInfoKey] as! NSValue).CGRectValue()
        updateChildVCLayoutGuides()
    }
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
}

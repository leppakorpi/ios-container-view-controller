//
//  UIViewController+LayoutGuide.swift
//  CustomVCContainer
//
//  Created by Toni Leppäkorpi on 03/02/16.
//

import UIKit

extension UIViewController {
    
    func setTopLayoutGuideLength(length: CGFloat) {
        _ = self.view // load view if not already loaded
        if self.topLayoutGuide.length == length { return }
        removeHeightConstraintForLayoutGuide(self.topLayoutGuide)
        NSLayoutConstraint(item: self.topLayoutGuide, attribute: .Height, relatedBy: .Equal, toItem: .None, attribute: .NotAnAttribute, multiplier: 0.0, constant: length).active = true
    }
    
    func setBottomLayoutGuideLength(length: CGFloat) {
        _ = self.view // load view if not already loaded
        if self.bottomLayoutGuide.length == length { return }
        removeHeightConstraintForLayoutGuide(self.bottomLayoutGuide)
        NSLayoutConstraint(item: self.bottomLayoutGuide, attribute: .Height, relatedBy: .Equal, toItem: .None, attribute: .NotAnAttribute, multiplier: 0.0, constant: length).active = true
    }
    
    func removeHeightConstraintForLayoutGuide(layoutGuide: UILayoutSupport) {
        for constraint in self.view.constraints {
            if constraint.firstItem === layoutGuide && constraint.firstAttribute == .Height {
                constraint.active = false
                break
            }
        }
    }
}

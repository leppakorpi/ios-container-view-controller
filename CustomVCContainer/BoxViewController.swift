//
//  BoxViewController.swift
//  CustomVCContainer
//
//  Created by Toni Leppäkorpi on 02/02/16.
//

import UIKit

class BoxViewController: UIViewController {

    var label: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.backgroundColor = UIColor(hue: 0.4, saturation: 0.3, brightness: 0.8, alpha: 1.0)
        createLabel()
        createTopBox()
        createBottomBox()
    }

    func createLabel() {
        label = UILabel()
        label.text = "Child View Controller\n Long Label"
        label.textAlignment = .Center
        label.numberOfLines = 0
        
        label.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(label)
        
        let constraints: [NSLayoutConstraint] = [
            label.centerXAnchor.constraintEqualToAnchor(view.centerXAnchor),
            label.centerYAnchor.constraintEqualToAnchor(view.centerYAnchor),
            label.leadingAnchor.constraintGreaterThanOrEqualToAnchor(view.leadingAnchor),
            label.trailingAnchor.constraintLessThanOrEqualToAnchor(view.trailingAnchor)
        ]
        view.addConstraints(constraints)
    }
    
    func createTopBox() {
        let box = UIView()
        box.backgroundColor = UIColor(hue: 0.9, saturation: 0.8, brightness: 0.7, alpha: 1.0)
        box.translatesAutoresizingMaskIntoConstraints = false
        self.view.addSubview(box)
        
        box.heightAnchor.constraintEqualToConstant(50.0).active = true
        box.widthAnchor.constraintEqualToConstant(50.0).active = true
        box.topAnchor.constraintEqualToAnchor(topLayoutGuide.bottomAnchor).active = true
        box.centerXAnchor.constraintEqualToAnchor(self.view.centerXAnchor).active = true
    }
    
    func createBottomBox() {
        let box = UIView()
        box.backgroundColor = UIColor(hue: 0.7, saturation: 0.8, brightness: 0.7, alpha: 1.0)
        box.translatesAutoresizingMaskIntoConstraints = false
        self.view.addSubview(box)
        
        box.heightAnchor.constraintEqualToConstant(50.0).active = true
        box.widthAnchor.constraintEqualToConstant(50.0).active = true
        box.bottomAnchor.constraintEqualToAnchor(bottomLayoutGuide.topAnchor).active = true
        box.centerXAnchor.constraintEqualToAnchor(self.view.centerXAnchor).active = true
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
    }
    
}


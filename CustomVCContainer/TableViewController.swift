//
//  TableViewController.swift
//  CustomVCContainer
//
//  Created by Toni Leppäkorpi on 04/02/16.
//

import UIKit

class MyTextFieldCell: UITableViewCell {
    @IBOutlet var textField: UITextField!
}

class MyLabelCell: UITableViewCell {
    @IBOutlet var label: UILabel!
}

class TableViewController: UITableViewController, UITextFieldDelegate {

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.refreshControl = UIRefreshControl()
        self.refreshControl!.addTarget(self, action: "performRefresh:", forControlEvents: UIControlEvents.ValueChanged)
    }
    
    func performRefresh(sender: AnyObject) {
        let attrs = [NSForegroundColorAttributeName: UIColor.redColor()]
        let attributedTitle = NSAttributedString(string: "Refreshing", attributes: attrs)
        self.refreshControl!.attributedTitle = attributedTitle;
        
        delay(6.0) {
            self.refreshSucceeded()
        }
    }
    
    func refreshSucceeded() {
        guard let refreshControl = self.refreshControl else { return }
        if refreshControl.refreshing {
            refreshControl.endRefreshing()
        }
        
        let formatter = NSDateFormatter()
        formatter.dateStyle = .ShortStyle
        formatter.timeStyle = .ShortStyle
        let now = NSDate()
        let title = "Refreshed: \(formatter.stringFromDate(now))"
        
        let darkGreen = UIColor(hue: 0.33, saturation: 1.0, brightness: 0.5, alpha: 1.0)
        let attrs = [NSForegroundColorAttributeName: darkGreen]
        let attributedTitle = NSAttributedString(string: title, attributes: attrs)
        refreshControl.attributedTitle = attributedTitle;
    }
    
    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 10
    }
    
    override func tableView(tableView: UITableView, estimatedHeightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        if indexPath.row == 0 {
            return 50.0
        }
        return UITableViewAutomaticDimension
    }
    
    override func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return self.tableView(tableView, estimatedHeightForRowAtIndexPath: indexPath)
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        if indexPath.row == 0 {
            let cell = tableView.dequeueReusableCellWithIdentifier("fieldCell", forIndexPath: indexPath) as! MyTextFieldCell
            cell.textField.delegate = self
            return cell
            
        } else {
            let cell = tableView.dequeueReusableCellWithIdentifier("textCell", forIndexPath: indexPath) as! MyLabelCell
            cell.label.text = "\(indexPath.row) Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus euismod eros nunc. Phasellus sit amet pulvinar turpis, a dictum ipsum."
            return cell
        }
        
    }
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}

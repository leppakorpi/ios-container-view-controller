# CustomVCContainer #

Example iOS project that shows how to handle top and bottom layout guides and UITableViewController contentInset in container view controller.

![ccvcontainer.png](https://bitbucket.org/repo/kbx745/images/502441784-ccvcontainer.png)

![ccvcontainer2.png](https://bitbucket.org/repo/kbx745/images/4112848612-ccvcontainer2.png)